// FUNCTIONS

// Built in functions / methods
// Arguments / Parameters
// Arrow functions


// useful for repetitive code
// functions can take in data, do something with it, and spit out something else at the end


// Examples of built in functions
// console
// Math
// window
// querySelector

//console.clear()

//-------

// CUSTOM FUNCTIONS

// function slugify(str, casing = 'lower') {
//   const slug = str.replace(/ /g, '-');
//   if(casing === 'lower') {
//     return slug.toLowerCase();
//   }
//   return slug;
// }


// const slugify = str => str.replace(/ /g, '-').toLowerCase();


// console.log(slugify('Blinds and Shutters'));


// const cmToInches = cms => cms / 2.54;

// console.log(cmToInches(20));