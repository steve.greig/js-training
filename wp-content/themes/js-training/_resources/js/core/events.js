// EVENTS / Event Listeners

const btn = document.querySelector('.my-btn');

/* KEYBOARD EVENTS
-------------------------
keydown
keyup
*/


/* MOUSE EVENTS
-------------------------
click
dblclick
contextmenu
select
wheel
mouseover
mouseout
mouseenter
mouseleave
mousedown
mouseup
mousemove
*/


/* VIEWPORT EVENTS
-------------------------
resize
scroll
*/


/* FORM / INPUT EVENTS
-------------------------
change
focus
blur
submit
reset
*/


/* CSS TRANSITION EVENTS
-------------------------
animationstart
animationend
animationiteration
transitionstart
transitionend
transitionrun
transitioncancel
*/