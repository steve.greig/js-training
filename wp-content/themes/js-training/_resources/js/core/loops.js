// LOOPS

// forEach
// for of
// for
// for in
// while
// map
// filter
// reduce
// -------------------------------------------------

// Array
let ctcTeamA = ['Ste', 'Joe', 'Dan', 'Mark', 'Lauren', 'Chris']

// NodeList
let blocks = document.querySelectorAll('.blocks li');

// Object
let ctcTeamO = {
  Web: 'Ste',
  IM: 'Joe',
  Copy: 'Dan',
  PPC: 'Mark',
  Social: 'Lauren',
  Client: 'Chris',
}



// forEach
// -------------------------------------------------
// Arrays

// NodeLists

// Objects

// forEach arguments - item, index, original array



// for of
// -------------------------------------------------
// Arrays

// NodeLists

// Objects


// for
// -------------------------------------------------
// for running a block of code x amount of times

// requires 3 things
// initial expression (loop starts here) / condition / increment


// for in
// -------------------------------------------------
// Specifically for objects
// Returns the keys of your object items


// while
// -------------------------------------------------
// Run a block of code whilst the condition is true



// map
// -------------------------------------------------
// map is an array method
// takes in an array, performs an operation on each array item, and returns the modified array
// it will always return the same amount of items on the other side
// should't be used to modify the dom or anything like that, should just be used to return a modified version of your array


// filter
// -------------------------------------------------
// filter is an array method
// takes in an array, sees if each array item meets your condition, and adds it to the new array if it does


// reduce
// -------------------------------------------------
// reduce is an array method
// takes in an array like map/filter, but this one will return just a single value
// e.g. if you have an array of numbers, you could use reduce to find the total sum of those numbers

