// DATA TYPES

// 1) Strings
// 2) Numbers
// 3) Booleans
// 4) Objects (+ Arrays)
// 5) null
// 6) undefined



// STRINGS
// const firstName = "Tom";
// const lastName = "Nightingale";
// let fullName = "Hello my name is " + firstName + " " + lastName + ".";
// fullName = `Hello my name is ${firstName} ${lastName}.`;

// const multiLine = `my
// name
// is
// tom`

// console.log(multiLine);

// NUMBERS


// BOOLEANS
// let isHome = false;

// if(location.pathname === '/') {
//   isHome = true;
// }

// console.log(isHome);


// OBJECTS (+ Arrays)
// const person = {
//   name: "Tom",
//   age: 21,
//   startedIn: 2009,
//   lengthOfService: function(year) {
//     return year - this.startedIn;
//   },
//   hobbies: ['Football', 'Web Dev', 'Gran Turismo'],
// }


// person.hobbies.push('Cycling');
// person.hobbies.pop();
// person.hobbies.unshift('Cycling');
// person.hobbies.shift();

// person.hobbies = ['Cycling', ...person.hobbies];

// console.log(person.hobbies);

// .slice() - get a specified portion of the array
// .splice() - delete a specified portion of the array
// .includes() - determines whether an array contains a particular item, boolean
// .some() - does at least one item in array meet a given condition? boolean
// .every() - does every item in array meet a given condition? boolean
// .find() - find a particular array item (returns that array item)
// .findIndex() - find position in an array for a particular item
// .join() - combine all the array items into a single string
// .concat() - merge 2 or more arrays




// Null - essentially a value of nothing - intentionally assigned initial value
//let myVar = null;

// Undefined - doesn't have a value
//let myVar; // undefined